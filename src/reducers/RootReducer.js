import AuthReduder from './AuthReducer';
import ProjectReduder from './ProjectReducer';
import { combineReducers} from 'redux';
import { firestoreReducer } from 'redux-firestore';
import { firebaseReducer } from 'react-redux-firebase';

const RootReducer = combineReducers({
    auth : AuthReduder,
    project : ProjectReduder,
    firestore: firestoreReducer,
    firebase: firebaseReducer
})

export default RootReducer;