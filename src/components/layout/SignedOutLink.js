import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import { signOut } from '../../reducers/actions/authActions';


const  SignedOutLink = () => {
  return (
 
        <ul className="right">
            <li><NavLink to="/signup"> SignUp </NavLink></li>
            <li><NavLink to="/signin"> Login </NavLink></li>
        </ul> 
  );
}

const mapDispatchToProps = (dispatch) =>{
  console.log('signing out.........')
  return {
    signOut : () => dispatch(signOut())
  }
}

export default connect(null, mapDispatchToProps)(SignedOutLink);


