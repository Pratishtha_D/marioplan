import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

var firebaseConfig = {
    apiKey: "AIzaSyATLjVPnhs8RDUSqr4qydLYYe4TT3rWCZk",
    authDomain: "marioplan-284c9.firebaseapp.com",
    databaseURL: "https://marioplan-284c9.firebaseio.com",
    projectId: "marioplan-284c9",
    storageBucket: "marioplan-284c9.appspot.com",
    messagingSenderId: "287921316406",
    appId: "1:287921316406:web:4032bdfdcd117fc2f3ae97"
  };

   // Initialize Firebase
   firebase.initializeApp(firebaseConfig);

   firebase.firestore();

   export default firebase;